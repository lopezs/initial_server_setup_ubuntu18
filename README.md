# Automating Initial Server Setup of Ubuntu 18.04 on a DigitalOcean droplet

Can be done during or after the provisioning

The script performs the following actions:

- Create a regular user account with sudo privileges using the name specified by the `USERNAME` variable.
Configure the initial password state for the new account. 
- If the server was configured for password authentication, the original, generated administrative password is moved from the root account to the new sudo account. The password for the root account is then locked.
- If the server was configured for SSH key authentication, a blank password is set for the sudo account.
- The sudo user’s password is marked as expired so that it must be changed upon first login.
- The `authorized_keys` file from the root account is copied over to the sudo user if `COPY_AUTHORIZED_KEYS_FROM_ROOT` is set to true.
- Any keys defined in `OTHER_PUBLIC_KEYS_TO_ADD` are added to the sudo user’s `authorized_keys` file.
- Password-based SSH authentication is disabled for the root user.
- The UFW firewall is enabled with SSH connections permitted.

The following variables affect how the script is run:

 - `USERNAME`: The name of the regular user account to create and grant sudo privileges to.
 - `COPY_AUTHORIZED_KEYS_FROM_ROOT`: Whether to copy the SSH key assets from the root account to the new sudo account.
 - `OTHER_PUBLIC_KEYS_TO_ADD`: An array of strings representing other public keys to add to the sudo-enabled account. This can optionally be used in addition to or instead of copying the keys from the root account.
 - `INSTALL_DOCKER`: Whether to install docker on the droplet

## 1. Set up During Droplet Creation process

 - Tick "User data" and paste the contents of the file `initial_server_setup.sh` into the box, amend as needed
 - Finish provisioning as normal, any SSH keys added will be copied to the SUDO user. Allow a few minutes for the script to complete, before trying to SSH into the server as the SUDO user

## 2. Set up After provisioning

 - Log in as root and `curl -L https://gitlab.com/lopezs/initial_server_setup_ubuntu18.sh/raw/master/initial_server_setup.sh -o /tmp/initial_setup.sh`
 - edit `/tmp/initial_setup.sh` and set variables as needed
 - perform the setup `bash /tmp/initial_setup.sh` 
